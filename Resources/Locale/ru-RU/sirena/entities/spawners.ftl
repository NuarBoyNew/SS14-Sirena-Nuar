ent-RandomHumanoidSpawnerERTLeaderSirena = Лидер ОБР
   .desc = { ent-MarkerBase.desc }
   .suffix = Только люди, EVA
ent-SpawnerERTLeaderSirena = { ent-RandomHumanoidSpawnerERTLeaderSirena }
   .desc = { ent-MarkerBase.desc }
   .suffix = Только люди, EVA, Сетка

ent-RandomHumanoidSpawnerERTSecuritySirena = СБ ОБР
   .desc = { ent-MarkerBase.desc }
   .suffix = Только люди, EVA
ent-SpawnerERTSecuritySirena = { ent-RandomHumanoidSpawnerERTSecuritySirena }
   .desc = { ent-MarkerBase.desc }
   .suffix = Только люди, EVA, Сетка

ent-RandomHumanoidSpawnerERTMedicalSirena = Медик ОБР
   .desc = { ent-MarkerBase.desc }
   .suffix = Только люди, EVA
ent-SpawnerERTMedicalSirena = { ent-RandomHumanoidSpawnerERTMedicalSirena }
   .desc = { ent-MarkerBase.desc }
   .suffix = Только люди, EVA, Сетка

ent-RandomHumanoidSpawnerERTEngineerSirena = Инженер ОБР
   .desc = { ent-MarkerBase.desc }
   .suffix = Только люди, EVA
ent-SpawnerERTEngineerSirena = { ent-RandomHumanoidSpawnerERTEngineerSirena }
   .desc = { ent-MarkerBase.desc }
   .suffix = Только люди, EVA, Сетка

ent-RandomHumanoidSpawnerERTJanitorSirena = Уборщик ОБР
   .desc = { ent-MarkerBase.desc }
   .suffix = Только люди, EVA
ent-SpawnerERTJanitorSirena = { ent-RandomHumanoidSpawnerERTJanitorSirena }
   .desc = { ent-MarkerBase.desc }
   .suffix = Только люди, EVA, Сетка

ent-RandomHumanoidSpawnerCBURNUnitLeaderSirena = Лидер ОБХАЗ
   .desc = { ent-MarkerBase.desc }
ent-SpawnerCBURNUnitLeaderSirena = { ent-RandomHumanoidSpawnerCBURNUnitLeaderSirena }
   .desc = { ent-MarkerBase.desc }
   .suffix = Только люди, Сетка

ent-RandomHumanoidSpawnerCBURNUnitSirena = Агент ОБХАЗ
   .desc = { ent-MarkerBase.desc }
ent-SpawnerCBURNUnitSirena = { ent-RandomHumanoidSpawnerCBURNUnitSirena }
   .desc = { ent-MarkerBase.desc }
   .suffix = Только люди, Сетка

ent-RandomHumanoidSpawnerDeadsquadSirena = Эскадон смерти
   .desc = { ent-MarkerBase.desc }
ent-SpawnerDeadsquadSirena = { ent-RandomHumanoidSpawnerDeadsquadSirena }
   .desc = { ent-MarkerBase.desc }
   .suffix = Только люди, Сетка

ent-SpawnerBorgChassisERTSirena = Киборг ОБР
   .desc = { ent-MarkerBase.desc }
   .suffix = Сетка

ent-BorgRandomSpawnSirena = Спавнер киборгов рандомный
   .desc = { ent-MarkerBase.desc }
   .suffix = Сетка

ent-SpawnerBorgChassisSyndSecSirena = Киборг Охраны Синдиката
   .desc = { ent-MarkerBase.desc }
   .suffix = Сетка

ent-SpawnerBorgChassisSyndMedSirena = Киборг Медик Синдиката
   .desc = { ent-MarkerBase.desc }
   .suffix = Сетка

ent-SpawnerBorgChassisSyndEngSirena = Киборг Инженер Синдиката
   .desc = { ent-MarkerBase.desc }
   .suffix = Сетка

ent-SpawnerBorgChassisServiceSirena = Киборг Сервиса
   .desc = { ent-MarkerBase.desc }
   .suffix = Сетка

ent-SpawnerBorgChassisMiningSirena = Киборг Шахтерский
   .desc = { ent-MarkerBase.desc }
   .suffix = Сетка

ent-SpawnerBorgChassisMedicalSirena = Киборг Медицинксий
   .desc = { ent-MarkerBase.desc }
   .suffix = Сетка

ent-SpawnerBorgChassisJanitorSirena = Киборг Уборщик
   .desc = { ent-MarkerBase.desc }
   .suffix = Сетка

ent-SpawnerBorgChassisEngineerSirena = Киборг Инженер
   .desc = { ent-MarkerBase.desc }
   .suffix = Сетка

ent-SpawnerBorgChassisGenericSirena = Киборг базовый
   .desc = { ent-MarkerBase.desc }
   .suffix = Сетка

ent-SpawnerBorgChassisScienceSirena = Киборг научный
   .desc = { ent-MarkerBase.desc }
   .suffix = Сетка

ent-SpawnerBorgChassisSecSirena = Киборг СБ
   .desc = { ent-MarkerBase.desc }
   .suffix = Сетка

ent-SpawnerBorgChassisUICSirena = Киборг СНК
   .desc = { ent-MarkerBase.desc }
   .suffix = Сетка
